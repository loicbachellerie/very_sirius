<?php
	require_once("action/LoginAction.php");
  $action = new LoginAction();
	$action->execute();
	require_once("partials/header.php");
	
?>
<!-- body -->
<script src="assets/js/login.js"></script>
<script src="assets/js/particles.js"></script>	
<script>particlesJS.load('particles-js', 'assets/particles.json');</script>


<div id="loginContainer" class="">
	<div id="volume"><i class="fa fa-volume-up fa-2x color"></i></div>
	<div id="appearTitle"><h2></h2></div>
	<div id="errorDiv"><h2 id="errorMessage"></h2></div>
	<div id="particles-js"></div>
	<div class="login move"><!-- absolute -->
				<div class="loginTitle text-center noSelect"><h2>Connectez-vous</h2></div>
				<a href="http://apps-de-cours.com/web-sirius/server/stats.php" target="_blank">
					<p id="connections" class="text-center noSelect"></p>
				</a>
	<div id="form">
			<div class="form-group">
				<input type="text" class="form-control" id="username" placeholder="Username">
			</div>
			<div class="form-group">
				<input type="password" class="form-control" id="password" placeholder="Password">
			</div>
			<div class="form-group text-center">
				<button id="connect" type="submit" class="btn btn-primary">CONNEXION</button>
			</div>
			
		</div>
  </div>
</div>

<!-- body -->
<?php
require_once("partials/footer.php");
?>
