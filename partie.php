<?php
	require_once("action/PartieAction.php");
  $action = new PartieAction();
	$action->execute();
	require_once("partials/header.php");
?>
<!-- body -->
<script src="assets/js/partie.js"></script>
<script src="assets/js/ajaxPartie.js"></script>

<canvas id="canvasPartie">
	<p>Votre navigateur ne supporte pas les canvas</p>
</canvas>

<img id="glowLeft" width="50" src="assets/img/glowtube.png"/>
<img id="glowRight" width="50" src="assets/img/glowtube.png"/>
<div id="reload" class="color"></div>
<div id="errorDiv"><h2 id="errorMessage"></h2></div>
<div id="volume"><i class="fa fa-volume-up fa-2x color"></i></div>
<div id="hpEnemy" class="c100 p100 dark orange">
		<span>100%</span>
		<div class="slice">
			<div class="bar"></div>
			<div class="fill"></div>
		</div>
</div>

<div id="playerScreen" class="col-lg-10 offset-lg-1">
	<div class="row alignCenter" >
		<div class="col-lg-4">
			<div class="progress" style="height: 20px;">
				<div id="hpBar" class="progress-bar bg-danger" role="progressbar" style="width:100%">HP : 100%</div>
			</div>
		</div>
		<div class="col-lg-4 text-center">
			<button id="Normal" type="button" class="attaque btn btn-primary">GH3TT0-BL45T3R5</button>
			<button id="Special1" type="button" class="attaque btn btn-danger">B3R3TT4-LZR</button>
			<button id="Special2" type="button" class="attaque btn btn-info">GR4V1TY-C0LT</button>
		</div>
		<div class="col-lg-4">
			<div class="progress" style="height: 20px;">
				<div id="mpBar" class="progress-bar bg-success" role="progressbar" style="width: 100%;">MP : 100%</div>
			</div>
		</div>
	</div>
	
</div>

<!-- body -->
<?php
require_once("partials/footer.php");
?>
