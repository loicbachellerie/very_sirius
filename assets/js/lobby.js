$(document).ready(function(){
	
	getUserInfo();
	
	
		var musiqueFond = new Audio("assets/sound/elevator-song.mp3");
	musiqueFond.play();
	var volume = true;
	
	$("#volume").on('click', function (e) {
		if(volume){
			musiqueFond.pause();
			$(this).children('i').removeClass('fa-volume-up color').addClass('fa-volume-off gris');
			volume = false;
		}
		else {
			musiqueFond.play();
			$(this).children('i').removeClass('fa-volume-off gris').addClass('fa-volume-up color');
			volume = true;
		}
	});
		
	
	
	
	
 	setTimeout(getListe, 2000);
	
	function getListe(){
	 $.ajax({
					url:"ajax-list.php",
					method:"post",
					data: {'service': "list"},
					dataType:"text",
			}).done(function(data){
		 		$("#content").text("");
		 		$.each(JSON.parse(data), function(key, value){
						var unePartie = '<div class="col-lg-3 partie">'+
															'<div class="card">'+
																'<div class="card-body text-center color2">'+
																	'<h4 class="card-title">'+value.name+'</h4>'+
																	'<h1 class="card-text">'+value.level+'</h1>'+
																	'<p class="card-text">Joueurs : '+value.nb+' / '+value.max_users+'</p>'+
																	'<p class="card-text">'+value.current_hp+' HP</p>'+
																	'<p class="card-text">'+value.type+'</p>'+
																	'<a href="#" id="'+value.id+'" class="btn btn-primary enter"><strong>ENTRER</strong></a>'+
																'</div>'+
															'</div>'+
														'</div>';
						$("#content").append(unePartie);
					});
		 			
					setTimeout(getListe, 2000);
			});
	}// FIN getListe();
	
	function getUserInfo(){
	 $.ajax({
					url:"ajax-user-info.php",
					method:"post",
					data: {'service': "user-info"},
					dataType:"text",
			}).done(function(data){
		 			if(data.length != 0){
						var infos = JSON.parse(data);
		 			 	$("#nom").text("Nom : "+infos.username);
						$("#hp").text("HP : "+infos.hp);
						$("#mp").text("MP : "+infos.mp);
						$("#niveau").text("NIVEAU : "+infos.level);
						$("#experience").text("EXPERIENCE : "+infos.level);
					}
			});
	}// FIN getUserInfo();
	
	
	
	$(document).on('click', '.enter', function(){
		var id = $(this).attr('id');
		$.ajax({
					url:"ajax-partie.php",
					method:"post",
					data: {'service': "enter", 
								 'id' : id
								},
					dataType:"text",
			}).done(function(data){
		 			if(JSON.parse(data) == "GAME_ENTERED")
						$(location).attr('href', 'partie.php');
			});
	})
	
	
	
});