$(document).ready(function(){
	
		if (localStorage["username"] != null) {
			$('#username').val(localStorage["username"]);
		}
	
	var musiqueFond = new Audio("assets/sound/Van_Halen_Jump.mp3");
	musiqueFond.play();
	var volume = true;
	
	$("#volume").on('click', function (e) {
		if(volume){
			musiqueFond.pause();
			$(this).children('i').removeClass('fa-volume-up color').addClass('fa-volume-off gris');
			volume = false;
		}
		else {
			musiqueFond.play();
			$(this).children('i').removeClass('fa-volume-off gris').addClass('fa-volume-up color');
			volume = true;
		}
	});
		
	
	/********** GET LES USERS CONNECTÉS **********/
	$.ajax({
			url:"https://loicbachellerie.com/sirius/get.php",
			method:"post",
			data: {'getConnected': 'qazwsxedcrfvtgbyhnujmikolp'},
			dataType:"text",
	}).done(function(data){
		
		var phrase = "";
		
		if(data == 0)
			phrase = "Aucun autre utilisateur connecté";
		else if(data == 1)
			phrase = "Il y a "+data+" utilisateur connecté";
		else 
			phrase = "Il y a "+data+" utilisateurs connectés";
		
		$("#connections").text(phrase).slideDown(500);
		
	}).fail(function(data){
		afficherMsg("Erreur appel ajax");
	});
 	
	
	$(document).keypress(function(e){
    if (e.which == 13){
        $("#connect").click();
    }
	});
	
	/********** CONNEXION **********/
	$("#connect").on('click', function (e) {
		var service = 'signin';
	    $.ajax({
				url:"ajax-login.php",
				method:"post",
				data: {'service': service,
							 'username': $('#username').val(),
							 'password': $('#password').val()
							},
				dataType:"text",
		}).done(function(data){
				if(JSON.parse(data).length == 40){
					/**** LOCALSTORAGE ****/
					localStorage["username"] = $('#username').val();
					$("#formConnection").addClass('rotateForm');
					$("#loginContainer").animate({ 'marginTop': "-"+$("#loginContainer").outerHeight()+"px"}, 1200, function(){
					$(location).attr('href', 'lobby.php');
					});
				}
				else{
					afficherMsg(JSON.parse(data));
				}
			});
	});
	
	function afficherMsg(message) {
     $("#errorMessage").text(message);
		 $("#errorDiv").slideDown(2000).delay(3000).slideUp(2000);
	}
	
	
	/********** SUIVI DU CURSEUR ***********/
  var moving = true;
	var selector = $("#loginContainer");
	var xAngle = 20;
	var yAngle = 0;
	var zValue = 50; //décolle le div en profondeur
  var duration = "200ms";
  loginTransform();
  
	function loginTransform(){	
		selector.on("mousemove",function(e){
		  var XRel = e.pageX - $(this).offset().left;
			var YRel = e.pageY - $(this).offset().top;
			var width = $(this).width();
			yAngle = -(0.5 - (XRel / width)) * 30; 
			xAngle = (0.4 - (YRel / width)) * 30;
			
			if(moving) updateView($(this).children(".move"));
    });
		

		selector.on("mouseleave",function(){ /***** reset toutes les Transformations *****/
			reset(selector);	
		});

		selector.children('.move').on("click",function(){ /***** reset toutes les Transformations *****/
			moving = false;
			reset(selector);
		});
		
  }
  	
	function reset(selector){
		var div = selector.children(".move");
		div.css({"transform":"perspective(1000px) translate3d(-50%, -50%, 0px) rotateX(0deg) rotateY(0deg)","transition":"all "+duration+" linear 0s","-webkit-transition":"all "+duration+" linear 0s"});
		}
	
	function updateView(div){
		div.css({"transform":"perspective(1000px) translate3d(-50%,-50%,"+zValue+"px) rotateX(" + xAngle + "deg) rotateY(" + yAngle + "deg)","transition":"all "+duration+" linear 0s","-webkit-transition":"all "+duration+" linear 0s"});
		}	
  
});
