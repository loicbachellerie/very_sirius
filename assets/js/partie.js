$(document).ready(function(){

	
	var musiqueFond = new Audio("assets/sound/daft_punk_veridis_quo.mp3");
	musiqueFond.play();
	var volume = true;
	
	$("#volume").on('click', function (e) {
		if(volume){
			musiqueFond.pause();
			$(this).children('i').removeClass('fa-volume-up color').addClass('fa-volume-off gris');
			volume = false;
		}
		else {
			musiqueFond.play();
			$(this).children('i').removeClass('fa-volume-off gris').addClass('fa-volume-up color');
			volume = true;
		}
	});
	
	
	var canvas, ctx, sprites, boss,
			width = window.innerWidth,
			height = window.innerHeight;
	
	// TOUCHES
	var espace = false,
			gauche = false,
			droite = false,
			haut = false,
			bas = false,
			pressTime = 0;  //VARIER LES SPRITES SELON LA DURÉE D'UN VIRAGE
	
	// SHIP
  var ship_width = 95, // LARGEUR D'UN SPRITE
			ship_height = 150, // HAUTEUR D'UN SPRITE
			posX = (width / 2) - ship_width/2, 
			posY = height - ship_height, 
			srcX = 10, srcY = 0;
	
	var boss_width = 610,
			boss_height = 610,
			bossPosX = (width / 2) - 200/2,
			bossPosY = height/2 - 200,
			sourceX = 0, sourceY = 0;
			
	// SHOOT - COOLDOWN ANIMATION
	var arcs = [];
	var color = "#FFDA44";
	var bgcolor = "#03a9f4";
	
	var firstGo = true;
	var last = "bas";

	
	function turn(){
		pressTime = 0;
		gauche = false;
		droite = false;
		haut = false;
		bas = false;
		
		if(last == "bas"){
			droite = true;
			haut = true;
			last = "right";
		}else if(last == "right"){
			haut = true;
			last = "haut";
		}
		else if(last =="haut"){
			gauche = true;
			last = "gauche";
		}
		else if(last == "gauche"){
			bas = true;
			last = "bas";
		}
		if(firstGo){
			setTimeout(turn, 1000);
			firstGo = false;
		}
		else
			setTimeout(turn, 2000);
	}
	
	turn();
	
	init();
	
	function init(){
		canvas = document.getElementById('canvasPartie');
		canvas.width = width;
		canvas.height = height;
	  ctx = canvas.getContext('2d');
		initShip();
		setInterval(tick, 1000/60);
	}

		function initShip(){
	  sprites = new Image(); 
		sprites.src = 'assets/img/ships.png';
		boss = new Image();
		boss.src = 'assets/img/bossSprite.png';
			
		document.addEventListener('keydown', keyDown, false);
		document.addEventListener('keyup', keyUp, false);
	}
	
	function dessinerBoss(){
		if(pressTime < 3) sourceX = boss_width;
		else if(pressTime < 6) sourceX = 2*boss_width;
		else if(pressTime < 9) sourceX = 3*boss_width;
		else if(pressTime < 12) sourceX = 4*boss_width;
		else if(pressTime <= 15) sourceX = 0;
		ctx.drawImage(boss,sourceX,sourceY,boss_width,boss_height, bossPosX, bossPosY, 200, 200);
		
	}

	function dessinerShip(){		
		// 60 refresh/seconde, 5 sprites en 1/4 secondes = (60/4)/5 = 3 refresh par image; 
		if(gauche){
			if(posX-5 > 5){ // Bordures
				posX -= 2;
				if(pressTime < 3) srcX = 4*ship_width;
				else if(pressTime < 6) srcX = 3*ship_width;
				else if(pressTime < 9) srcX = 2*ship_width;
				else if(pressTime < 12) srcX = 1*ship_width;
				else if(pressTime <= 15) srcX = 0;
				
				if(pressTime < 15) pressTime +=1;
			}
		}
		else if(droite) {
			if(posX+5 < width - ship_width){ 
				posX += 2;
				if(pressTime < 3) srcX = 6* ship_width;
				else if(pressTime < 6) srcX = 7*ship_width;
				else if(pressTime < 9) srcX = 8*ship_width;
				else if(pressTime < 12) srcX = 9*ship_width;
				else if(pressTime <= 15) srcX = 10*ship_width;
				
				if(pressTime < 15) pressTime +=1;
			}
		}
		else if(haut){
			 console.log("haut");
			 if(posY-5 > height - ship_height -200)
				 posY -= 5;
			}
		else if(bas){
			console.log("bas");
			if(posY+5 < height - ship_height - 100)
				posY += 5;
		}

		ctx.drawImage(sprites,srcX,srcY,ship_width,ship_height,posX,posY,ship_width,ship_height);
		if (droite === false || gauche === false)
			srcX = 5*ship_width; //Image du vaisseau a plat

	}//END dessinerShip()

	function tick(){
	ctx.clearRect(0,0, width, height); // width et height du canvas tout entier
	dessinerShip();
	dessinerBoss();
	}
	
	function shoot(){
		var shootSound = new Audio("assets/sound/shoot.wav");
		shootSound.play();
	}
	
	function keyDown(e){
		if(e.keyCode == 32) shoot();
		else if (e.keyCode == 39) droite = true;
		else if (e.keyCode == 37) gauche = true;
		else if(e.keyCode == 38) haut = true;
		else if(e.keyCode == 40) bas = true;
	}
	
	function keyUp(e){
		pressTime = 0;
		if (e.keyCode == 39) droite = false;
		else if (e.keyCode == 37) gauche = false;
		else if(e.keyCode == 38) haut = false;
		else if(e.keyCode == 40) bas = false;
	}

	function degToRad (degrees) {
  return degrees * (Math.PI/180);     
}
		
});
