
var width = window.innerWidth;
var height = window.innerHeight;
let ctx;
let spriteList = [];

$(document).ready(function(){
	
			/********** ECRIRE SIRIUS **********/
	var i = 0;
	var txt = 'SIRIUS';
	var speed = 200;
	var cursor = false;

	function typeWriter() {
		if (i < txt.length) {
			var text = $("#lobbyTitle").text();
			$("#lobbyTitle").text(text+txt.charAt(i));
			i++;
			setTimeout(typeWriter, speed);
		}
	}
	typeWriter();
	/********** FIN ECRIRE SIRIUS **********/
	
	$("#infosJoueur").fadeIn(1200);
	
	var canvas = document.getElementById("canvasLobby");
	canvas.width = width;
	canvas.height = height;
	ctx = canvas.getContext("2d");
	spriteList.push(new Background());
	mainTick();
	
});

function mainTick() {
	if (Math.random() < 0.5) {
		spriteList.push(new Etoile());
	}

	for (var i = 0; i < spriteList.length; i++) {
		
		const element = spriteList[i];
		const alive = element.tick();
		
		if (!alive) {
			spriteList.splice(i, 1);
			i--;
		}
	}

	window.requestAnimationFrame(mainTick);
}
	