class Etoile {
	constructor() {
		this.x = Math.random() * window.innerWidth;
		this.y = - Math.random()*2;
		this.size = Math.random() * 2 + 0.5;
		this.speedY = Math.random() * 16 + 4;
		this.speedX = 0;
	}

	tick() {
		let alive = true;

		this.x += this.speedX;
		this.y += this.speedY;
		
		if(this.speedY > 12 && this.speedY < 15 ){
			ctx.fillStyle = "#FFDA44";
		}
		else if(this.speedY > 8 && this.speedY < 12){
			ctx.fillStyle = "#e7002f";
		}
		else if(this.speedX < 8){
			ctx.fillStyle = "lightgrey";
		}
		
		//ctx.arc(this.x, this.y,this.size, 0, 2*Math.PI);
		
		
		ctx.fillRect(this.x, this.y, this.size, this.size);

		if (this.y > window.innerHeight) {
			alive = false;
		}

		return alive;
	}
}