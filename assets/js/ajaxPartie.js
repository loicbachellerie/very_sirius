var reload = false;

$(document).ready(function(){
	
 	setTimeout(getUserInfo, 2000);
	
	/*
	
"EMPTY_KEY"
"USER_NOT_FOUND"
"GAME_NOT_FOUND_WIN"
"GAME_NOT_FOUND_LOST"
"GAME_NOT_FOUND_NONE"
"TOO_MANY_CALLS_BAN"

	
	*/
	
	var lastHpEnemy = 100;
  
  function getUserInfo(){

	 $.ajax({
					url:"ajax-user-info.php",
					method:"post",
					data: {'service': "state"},
					dataType:"text",
			}).done(function(data){
						console.log(data);
						var infos = JSON.parse(data);
						console.log(infos);
						if (typeof infos !== "object") {
							if (infos == "GAME_NOT_FOUND_LOST") {
								afficherMsg("VOUS AVEZ PERDU !");
								quitterPartie();
							}
							else if(infos == "GAME_NOT_FOUND_WIN"){
								afficherMsg("VOUS AVEZ GAGNÉ !");
								quitterPartie();
							}
							else{
								afficherMsg(infos);
								quitterPartie();
							}
						}
						else {
							console.log("object");
							console.log(infos.player.hp);
							$("#hpBar").css({width:(infos.player.hp*100)/infos.player.max_hp+"%"});
							$("#hpBar").text("HP : "+Math.floor((infos.player.hp*100)/infos.player.max_hp)+"%");
							$("#mpBar").css({width:(infos.player.mp*100)/infos.player.max_mp+"%"});
							$("#mpBar").text("MP : "+Math.floor((infos.player.mp*100)/infos.player.max_mp)+"%");
							$("#hpEnemy").removeClass('p'+lastHpEnemy).addClass('p'+Math.floor((infos.game.hp*100)/infos.game.max_hp));
							$("#hpEnemy span").text(Math.floor((infos.game.hp*100)/infos.game.max_hp)+"%");
							lastHpEnemy = Math.floor((infos.game.hp*100)/infos.game.max_hp);	
						}
			
		 	setTimeout(getUserInfo, 2000);
		 
			});
	}// FIN getUserInfo();
	
	if(reload == false){
		
		$(".attaque").on('click', function (e) {
			shoot();
			var service = 'action';
			var skill = $(this).attr('id');
			console.log(skill);
				$.ajax({
					url:"ajax-partie.php",
					method:"post",
					data: {'service': service,
								 'skill-name':skill},
					dataType:"text",
			}).done(function(data){
					var response = JSON.parse(data);
					afficherReload();
				});
		});
	}
	
}); //document.onload

	function shoot(){
		var shootSound = new Audio("assets/sound/shoot.wav");
		shootSound.play();
	}

	function afficherReload(){
		reload = true;
		$("#reload").text("RELOADING");
		$("#reload").slideDown(2000, function(){reload = false;}).slideUp(100);
	}

	function afficherMsg(message) {
     $("#errorMessage").text(message);
		 $("#errorDiv").slideDown(2000).delay(3000).slideUp(2000);
	}

	function quitterPartie(){
		setTimeout(function(){location.href="lobby.php"} , 3000);   
	}




/*
 DATA AJAX
{

"game":
  { "name":"Void 1",
    "type":"Melee",
    "level":"0",
    "last_target":"JoeKingman",
    "hp":867,
    "max_hp":1000,
    "attacked":false
  },
    
"player":
  { "name":"toosirius",
    "level":1,
    "type":"Magic",
    "victories":0,
    "skills":[
    
             ],
    "hp":10,
    "mp":10,
    "max_hp":10,
    "max_mp":10
   },
 
 "other_players":[
                   {
                     "name":"JoeKingman",
                     "level":"1",
                     "welcome_text":"#1 On The Field, #1 In Your Heart",
                     "type":"Melee",
                     "victories":"1",
                     "max_hp":25,
                     "max_mp":5,
                     "hp":25,
                     "mp":5,
                     "attacked":"--"
                   }
                 ]
 }

*/
 
 

