<?php
	require_once("action/LobbyAction.php");
  $action = new LobbyAction();
	$action->execute();
	require_once("partials/header.php");
?>

<!-- body -->
<script src="assets/js/lobby.js"></script>
<script src="assets/js/sprite/Background.js"></script>
<script src="assets/js/sprite/Etoile.js"></script>
<script src="assets/js/lobbyAnimation.js"></script>
<canvas id="canvasLobby">
	<p>Votre navigateur ne supporte pas les canvas</p>
</canvas>

<div id="volume"><i class="fa fa-volume-up fa-2x color"></i></div>
<div class="container-fluid">
	<h1 id="lobbyTitle" class="lobbyTitle color text-center"></h1>
	<div id="infosJoueur" class="col-lg-4 offset-lg-4" style="display:none;">
		<div class="card partie">
  		<div class="card-body text-center">
				<h5 id="nom" class="card-title"></h5>
					<div class="row color">
						<div id="hp" class="col-lg-6"></div>
						<div id="mp" class="col-lg-6"></div>
						<div id="niveau" class="col-lg-6"></div>
						<div id="experience" class="col-lg-6"></div>
					</div>
  		</div>
		</div>
	</div>
	<div class="col-lg-8 offset-lg-2">
		<div id="content" class="row">
			<!-- Ajax viendra remplir ce content avec les parties disponibles  -->
		</div>
	</div>
</div>

<!-- body -->
<?php
require_once("partials/footer.php");
?>
