<?php
	require_once("action/CommonAction.php");

	class AjaxUserInfoAction extends CommonAction {
    public $result;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			
			if(!empty($_POST['service'])){
        $service = $_POST['service'];
	      if(!empty($_SESSION['key'])){
          if(strlen($_SESSION['key']) == 40){
							$data = [];
							$data["key"] = $_SESSION['key'];
							$this->result = CommonAction::callAPI($service, $data);
          }
          else
           $this->result = "La clé est incorrecte";
        }

			}
		}
	}
