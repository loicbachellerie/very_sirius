<?php
	require_once("action/CommonAction.php");

	class AjaxPartieAction extends CommonAction {
    public $result;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}
    
		protected function executeAction() {
			
			if(!empty($_POST['service'])){
        $service = $_POST['service'];
				
				/**** ENTER ****/
				if($service == "enter"){
					if(!empty($_SESSION['key']) && !empty($_POST['id'])){
						if(strlen($_SESSION['key']) == 40){
								$data = [];
								$data["key"] = $_SESSION['key'];
								$data["id"] = $_POST["id"];
								$this->result = CommonAction::callAPI($service, $data);
						}
						else $this->result = "La clé est incorrecte";
					}  
        }
				
				/**** ACTION ****/
				else if($service == "action"){
					if(!empty($_SESSION['key']) && !empty($_POST['skill-name'])){
						if(strlen($_SESSION['key']) == 40){
							$data = [];
							$data["key"] = $_SESSION['key'];
							$data["skill-name"] = $_POST["skill-name"];
							$this->result = CommonAction::callAPI($service, $data);
						}
						else $this->result = "La clé est incorrecte";
					}
				}
		}
	}
}


