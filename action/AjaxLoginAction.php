<?php
	require_once("action/CommonAction.php");

	class AjaxLoginAction extends CommonAction {
    public $result;
		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if(!empty($_POST["service"])){
				$service = $_POST["service"];
				if(!empty($_POST["username"]) && !empty($_POST["password"])){
					$data = [];
					$data["username"] = $_POST["username"];
					$data["pwd"] = $_POST["password"];
					$this->result = CommonAction::callAPI($service,$data);
					if(strlen($this->result) == 40){
						$_SESSION['key'] = $this->result;
						$_SESSION['visibility'] = 1;
					}
				}
			}
		}
	}
