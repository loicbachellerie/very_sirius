<?php
	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	session_start();

	abstract class CommonAction {
		public static $VISIBILITY_PUBLIC = 0;
		public static $VISIBILITY_MEMBER = 1;
		public static $VISIBILITY_ADMIN = 2;

		private $pageVisibility;

		public function __construct($pageVisibility) {
			$this->pageVisibility = $pageVisibility;
		}

		public function execute() {
			$this->executeAction();

		if (!empty($_GET["logout"])) {
			session_unset();
			session_destroy();
			session_start();
		}

		if (empty($_SESSION["visibility"])) {
			$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
		}

		if ($_SESSION["visibility"] < $this->pageVisibility) {
			header("location:login.php");
			exit;
			}
		}
		
		protected function callAPI($service, array $data) {
			$apiURL = "https://apps-de-cours.com/web-sirius/server/api/" . $service . ".php";

			$options = array(
				'http' => array(
						'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
						'method'  => 'POST',
						'content' => http_build_query($data)
				)
			);
			$context  = stream_context_create($options);
			$result = file_get_contents($apiURL, false, $context);

			if (strpos($result, "<br") !== false) {
			var_dump($result);
			exit;
			}

		return json_decode($result);
		}
		

		public static function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}

		public static function getUsername() {
			return empty($_SESSION["username"]) ? "Invité" : $_SESSION["firstname"];
		}
		
		

		protected abstract function executeAction();
	}
